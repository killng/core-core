require 'rails_helper'

RSpec.describe Animal, type: :model do
  subject(:animal) { build(:animal) }

  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to be_valid }

end
