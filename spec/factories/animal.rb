FactoryBot.define do
  factory :animal do
    name        { "#{Faker::Company.unique.name} #{rand(100_000)}" }
    diet        { "#{rand(100_000)} #{Faker::Company.unique.name}"[0..9].strip }
    status    { 'falabella' }

  end
end