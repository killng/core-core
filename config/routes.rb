Rails.application.routes.draw do
  get 'api_instructions', to: 'home#api_instructions', defaults: { format: :html }
  devise_for :users
  resources :characters do
    resources :comments, only: [:destroy,:new,:index,:create], module: :characters
    collection do 
      post 'acquire_characters'
    end
  end
  resources :animals do
    resources :comments, only: [:destroy,:new,:index,:create], module: :animals
    collection do 
      post 'acquire_animals'
    end
  end
  resources :comments, only: [:show,:index,:destroy]
  devise_scope :user do
	  authenticated :user do
	    root 'characters#index', as: :authenticated_root
	  end
		unauthenticated do
	   root "devise/sessions#new", as: :unauthenticated_root
	  end
  end
  ## API
  namespace :api , defaults: { format: 'json' } do
    namespace :v1 do
      resources :characters, except: [:edit,:new] do 
        collection do 
          post 'acquire_characters'
          get 'get_by_range'
        end
      end
    end
  end
end
