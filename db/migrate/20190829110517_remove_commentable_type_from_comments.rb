class RemoveCommentableTypeFromComments < ActiveRecord::Migration[5.2]
  def change
    remove_column :comments, :commentable_type
  end
end
