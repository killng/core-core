class AddCharacterIdToComments < ActiveRecord::Migration[5.2]
  def change
    add_column :comments, :character_id, :integer
  end
end
