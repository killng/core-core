class AddRankRangeToCharacters < ActiveRecord::Migration[5.2]
  def change
    add_column :characters, :rank_range, :integer
  end
end
