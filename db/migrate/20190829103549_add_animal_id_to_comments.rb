class AddAnimalIdToComments < ActiveRecord::Migration[5.2]
  def change
    add_column :comments, :animal_id, :integer
  end
end
