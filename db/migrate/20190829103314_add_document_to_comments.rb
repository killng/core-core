class AddDocumentToComments < ActiveRecord::Migration[5.2]
  def change
    add_column :comments, :document, :string
  end
end
