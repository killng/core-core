def rr(name)# random record
  if name == "user"
    User.where( id:(1+rand(User.count) ) ).last
  elsif name == "character"
    Character.where( id:(1+rand(Character.count) ) ).last
  elsif name == "animal"
    Animal.where( id:(1+rand(Animal.count) ) ).last
  end
end
normal_user = User.create(email: "normal@user.com", password: "123123", name: Faker::Name.name )

first_user = User.create(email: "g@g.g", password: "123123", name: Faker::Name.name )
#first_user.add_role(:admin)

second_user = User.create(email: "admin@admin.com", password: "123123", name: Faker::Name.name )
#second_user.add_role(:admin)
i = 0
30.times do 
  Character.create(name: "fake character #{i}", gender: "female", slug: "#{i}",rank: rand(300), house: "mario #{i}" )
  i +=1
end

i = 0
30.times do 
  Animal.create(name: "animal #{i}", status: "female", diet: "#{i}" )
  i +=1
end

20.times do
  Comment.create(user: first_user, commentable_id: rr('character').id,body: Faker::Quotes::Shakespeare.hamlet_quote, commentable_type: Character)
end

20.times do
  Comment.create(user: first_user, commentable_id: rr('animal').id,body: Faker::Quotes::Shakespeare.hamlet_quote, commentable_type: Animal)
end