class Animal < ApplicationRecord

  has_many :comments, as: :commentable, dependent: :destroy

  validates :name, presence: true
  validates_uniqueness_of :name


end
