class Comment < ApplicationRecord
  belongs_to :user
  belongs_to :commentable, polymorphic: true

  validates :body, :commentable_id, :commentable_type ,presence: true

  mount_uploader :document, DocumentUploader
  acts_as_paranoid

end
