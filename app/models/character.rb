class Character < ApplicationRecord

  has_many :comments, as: :commentable, dependent: :destroy

  enum rank_range: [:low_range, :medium_range, :high_range, :ultra_range]

  validates :name, presence: true
  validates_uniqueness_of :name

  after_create :assing_range

  scope :get_rank_range, ->(range) {
    Character.where(rank_range: range)
  }


  def assing_range
    if rank.to_i < 50 
      self.rank_range = 0
    elsif rank.to_i < 100
      self.rank_range = 1
    elsif rank.to_i < 150
      self.rank_range = 2
    else 
      self.rank_range = 3  
    end
    self.save!
  end

end
