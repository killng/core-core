class CommentsDatatable
  delegate :params, :h, :link_to, to: :@view

  include Rails.application.routes.url_helpers
  include ActionView::Helpers::OutputSafetyHelper

  def initialize(view,data)
    @view = view
    @comments = data
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: Comment.count,
      iTotalDisplayRecords: @comments.size,
      aaData: data
    }
  end

  private

  def data
    comments.map do |comment|
      delete_comment = "¿Are you sure want to delete this comment #{comment.body}?"
      [
        (comment.body.present? ? comment.body :  ''),
        (comment.user.present? ? comment.user.email : '' ),
        (comment.created_at.present? ? comment.created_at.strftime("%d-%m-%Y") : '' ),
        (comment.deleted_at.present? ? comment.deleted_at.strftime("%d-%m-%Y") : '' ),
        comment.deleted_at.nil? ? link_to('Delete', comment_path(comment), method: :delete, data: { confirm: delete_comment }, class: 'btn btn-xs btn-danger') : ''
        #link_to('Show', comment_path(comment), class: 'btn btn-xs btn-success')+" "+
        #link_to('Edit', edit_comment_path(comment), class: 'btn btn-xs btn-warning')
      ]
    end
  end

  def comments
    @comments = fetch_comments
  end

  def fetch_comments
    comments = @comments.order("#{sort_column} #{sort_direction}")
    comments = comments.page(page).per_page(per_page)   
    if params[:sSearch].present?
      params[:sSearch] = params[:sSearch].downcase
      comments = Comment.where("CAST(TO_CHAR(comments.created_at, 'dd-mm-YYYY') AS TEXT) LIKE :search OR LOWER(comments.body) LIKE :search ", search: "%#{params[:sSearch]}%")
    end
    comments
  end

  def page
    params[:iDisplayStart].to_i / per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = [ 'comments.body', '', 'comments.created_at', 'comments.deleted_at','comments.deleted_at']
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == 'desc' ? 'asc' : 'desc'
  end
end
