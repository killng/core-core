class AnimalsDatatable
  delegate :params, :h, :link_to, to: :@view

  include Rails.application.routes.url_helpers
  include ActionView::Helpers::OutputSafetyHelper

  def initialize(view,data)
    @view = view
    @animals = data
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: Animal.count,
      iTotalDisplayRecords: @animals.size,
      aaData: data
    }
  end

  private

  def data
    animals.map do |animal|
      delete_animal = "¿Are you sure want to delete this animal #{animal.name}?"
      [
        (animal.name.present? ? animal.name :  ''),
        (animal.diet.present? ? animal.diet : '' ),
        (animal.status.present? ? animal.status : '' ),
        (animal.created_at.present? ? animal.created_at.strftime("%d-%m-%Y") : '' ),
        #(animal.deleted_at.present? ? animal.deleted_at.strftime("%d-%m-%Y") : 'No' ),
        #animal.deleted_at.nil? ? link_to('Delete', animal_path(animal), method: :delete, data: { confirm: delete_animal }, class: 'btn btn-xs btn-danger') : ''
        link_to('Show', animal_path(animal), class: 'btn btn-xs btn-success')+" "+
        link_to('Edit', edit_animal_path(animal), class: 'btn btn-xs btn-warning')
      ]
    end
  end

  def animals
    @animals = fetch_animals
  end

  def fetch_animals
    animals = @animals.order("#{sort_column} #{sort_direction}")
    animals = animals.page(page).per_page(per_page)   
    if params[:sSearch].present?
      params[:sSearch] = params[:sSearch].downcase
      animals = Animal.where("CAST(TO_CHAR(animals.created_at, 'dd-mm-YYYY') AS TEXT) LIKE :search OR LOWER(animals.name) LIKE :search OR LOWER(animals.diet) LIKE :search OR LOWER(animals.status) LIKE :search ", search: "%#{params[:sSearch]}%")
    end
    animals
  end

  def page
    params[:iDisplayStart].to_i / per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = [ 'animals.name', 'animals.diet', 'animals.status','animals.created_at','']
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == 'desc' ? 'asc' : 'desc'
  end
end
