class CharactersDatatable
  delegate :params, :h, :link_to, to: :@view

  include Rails.application.routes.url_helpers
  include ActionView::Helpers::OutputSafetyHelper

  def initialize(view,data)
    @view = view
    @characters = data
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: Character.count,
      iTotalDisplayRecords: @characters.size,
      aaData: data
    }
  end

  private

  def data
    characters.map do |character|
      delete_character = "¿Are you sure want to delete this character #{character.name}?"
      [
        (character.name.present? ? character.name :  ''),
        (character.gender.present? ? character.gender : '' ),
        (character.slug.present? ? character.slug : '' ),
        (character.rank.present? ? character.rank : '' ),
        (character.rank_range.present? ? character.rank_range : '' ),
        (character.house.present? ? character.house : '' ),
        (character.created_at.present? ? character.created_at.strftime("%d-%m-%Y") : '' ),
        (character.image_url.present? ? 'Yes': 'No' ),
        #(character.deleted_at.present? ? character.deleted_at.strftime("%d-%m-%Y") : 'No' ),
        #character.deleted_at.nil? ? link_to('Delete', character_path(character), method: :delete, data: { confirm: delete_character }, class: 'btn btn-xs btn-danger') : ''
        link_to('Show', character_path(character), class: 'btn btn-xs btn-success')+" "+
        link_to('Edit', edit_character_path(character), class: 'btn btn-xs btn-warning')
      ]
    end
  end

  def characters
    @characters = fetch_characters
  end

  def fetch_characters
    characters = @characters.order("#{sort_column} #{sort_direction}")
    characters = characters.page(page).per_page(per_page)   
    if params[:sSearch].present?
      params[:sSearch] = params[:sSearch].downcase
      characters = Character.where("CAST(TO_CHAR(characters.created_at, 'dd-mm-YYYY') AS TEXT) LIKE :search OR LOWER(characters.name) LIKE :search OR LOWER(characters.house) LIKE :search ", search: "%#{params[:sSearch]}%")
    end
    characters
  end

  def page
    params[:iDisplayStart].to_i / per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = [ 'characters.name', 'characters.gender', 'characters.slug', 'characters.rank','characters.rank_range','characters.house','characters.created_at','characters.image_url','']
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == 'desc' ? 'asc' : 'desc'
  end
end
