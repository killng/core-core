class AnimalsController < ApplicationController

  include ApplicationHelper
  
  before_action :set_animal, only: [:edit, :update,:show, :destroy]
  def index
    @animals = Animal.all 
    respond_to do |format|
      format.html
      format.json { render json: AnimalsDatatable.new(view_context, @animals) }
    end
  end

  def show 
  end

  def edit
  end
  
  def create
    @animal = Animal.new(animal_params)
    respond_to do |format|
      if @animal.save
        format.html { redirect_to animals_path }
      else
        format.html { render :new }
      end
    end
  end
  
  def new
    @animal = Animal.new
  end
  
  def update
    respond_to do |format|
      if @animal.update(animal_params)
        format.html { redirect_to animal_path }
      else
        format.html { render :edit }
      end
    end
  end


  def destroy
    @animal = @animal.destroy 
    respond_to do |format|
      if @animal.errors.present?
        puts ap @animal.errors.messages
        format.html { redirect_to animals_url }
        format.json { head :no_content }
      else
        format.html { redirect_to animals_url}
        format.json { head :no_content }
      end
    end
  end
  
  def acquire_animals
    get_animal_data
    respond_to do |format|
      format.html { redirect_to animals_path }
    end
  end

  private
  def set_animal
    @animal = Animal.find(params[:id])
  end

  def animal_params
    params.require(:animal)
          .permit(:name, :diet, :status)
  end

end
