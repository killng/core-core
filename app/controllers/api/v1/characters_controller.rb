module Api
  module V1
    class CharactersController < V1Controller

      before_action :set_character, only: [:show, :update, :destroy]

      include ApplicationHelper

      def index
        render json: { status: :ok, characters: Character.all}
      end

      def show
        render json: { status: :ok, character: @character}
      end
      
      def create
    		@character = Character.new(character_params)
        if @character.save
          render json: { status: :ok, character: @character}
        else
          render json: { status: :bad_request}
        end
      end
      
      def update
        @character.assign_attributes(character_params)
        if @character.save
          render json: { status: :accepted, character: @character}
        else
          render json: { status: :bad_request}
        end
      end

      def destroy
        if @character != nil && @character.destroy
          render json: { status: :ok}
        else
          render json: { status: :bad_request}
        end
      end

      def acquire_characters
        get_data_json
        render json: { status: :ok }
      end

      def get_by_range
        characters = Character.get_rank_range(params[:range]) 
        if characters.size > 0
          render json: { status: :ok, characters: characters }
        else
          render json: { status: :bad_request }
        end
      end
      
      private 

      def set_character
        @character = Character.where( id: params[:id]).last
      end
      
      def character_params
        params.require(:character)
              .permit(:name, :gender, :slug, :rank, :house)
      end
    end
  end
end
